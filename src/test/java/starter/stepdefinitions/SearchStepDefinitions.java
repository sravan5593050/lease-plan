package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;
    private static String apiBaseUrl;

    @Given("the API base URL is {string}")
    public void theAPIBaseURLIs(String baseUrl) {
        apiBaseUrl = baseUrl;
    }


    public static String getApiBaseUrl() {
        return apiBaseUrl;
    }

    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String endpoint) {
        String fullUrl = getApiBaseUrl() + endpoint;
        SerenityRest.given().log().all().get(fullUrl);
    }

    @Then("he sees the results displayed for orange")
    public void heSeesTheResultsDisplayedForOrange() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for pasta")
    public void heSeesTheResultsDisplayedForPasta() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he sees the results displayed for cola")
    public void heSeesTheResultsDisplayedForCola() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Then("he does not see the results")
    public void heDoesNotSeeTheResults() {
        restAssuredThat(response -> response.body("error", contains("True")));
    }

    @Then("he does not see the results for mango")
    public void heDoesNotSeeTheResultsForMango() {
        restAssuredThat(response -> response.body("error", contains("True")));
    }

    @Then("he does not see the results for empty product")
    public void heDoesNotSeeTheResultsForEmptyProduct() {
        restAssuredThat(response -> response.body("error", contains("True")));
    }
}
