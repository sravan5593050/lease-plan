
Feature: Product Search

  Background:
    Given the API base URL is "https://waarkoop-server.herokuapp.com/api/v1/search/demo"


  Scenario: Search for a valid product
    When he calls endpoint "/orange"
    Then he sees the results displayed for orange

  Scenario: Search for another valid product
    When he calls endpoint "/apple"
    Then he sees the results displayed for apple

  @regression
  Scenario: Search for a different valid product
    When he calls endpoint "/pasta"
    Then he sees the results displayed for pasta

  Scenario: Search for yet another valid product
    When he calls endpoint "/cola"
    Then he sees the results displayed for cola

  Scenario: Search for an invalid product
    When he calls endpoint "/car"
    Then he does not see the results

  Scenario: Search for a non-existent product
    When he calls endpoint "/mango"
    Then he does not see the results

  Scenario: Search for an empty product
    When he calls endpoint "/"
    Then he does not see the results